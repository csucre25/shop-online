import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarbarComponent } from './carbar.component';

describe('CarbarComponent', () => {
  let component: CarbarComponent;
  let fixture: ComponentFixture<CarbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
