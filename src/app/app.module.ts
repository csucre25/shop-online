import { BrowserModule } from '@angular/platform-browser';
import { RoutingModule } from './app.routing';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TopbarComponent } from './shared/topbar/topbar.component';
import { ItemcardComponent } from './shared/itemcard/itemcard.component';
import { MainslideComponent } from './shared/mainslide/mainslide.component';
import { LeftbarComponent } from './shared/leftbar/leftbar.component';
import { CarbarComponent } from './shared/carbar/carbar.component';
import { ShopItemComponent } from './components/shop-item/shop-item.component';
import { ShopPageComponent } from './components/shop-page/shop-page.component';
import { ShopCarComponent } from './components/shop-car/shop-car.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ShopAboutComponent } from './components/shop-about/shop-about.component';
import { ShopContactComponent } from './components/shop-contact/shop-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    TopbarComponent,
    ItemcardComponent,
    MainslideComponent,
    LeftbarComponent,
    CarbarComponent,
    ShopItemComponent,
    ShopPageComponent,
    ShopCarComponent,
    FooterComponent,
    ShopAboutComponent,
    ShopContactComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
