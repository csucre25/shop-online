import { Component, OnInit } from '@angular/core';
import { MainslideComponent } from '../../shared/mainslide/mainslide.component';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.css']
})
export class ShopPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
