import { Routes, RouterModule } from '@angular/router';

import { ShopItemComponent } from './components/shop-item/shop-item.component';
import { ShopPageComponent } from './components/shop-page/shop-page.component';
import { ShopCarComponent } from './components/shop-car/shop-car.component';

const routes: Routes = [
    { path: '', component: ShopPageComponent },
    { path: 'shop-item', component: ShopItemComponent },
    { path: 'shop', component: ShopPageComponent },
    { path: 'shop-car', component: ShopCarComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const RoutingModule = RouterModule.forRoot(routes);